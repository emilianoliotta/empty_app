module FormHelper

 #---------------------------------------------------------------
 def icon(name, html_options={})
   html_options[:class] = ['fa', "fa-#{name}", html_options[:class]].compact
   content_tag(:i, nil, html_options)
 end

  #---------------------------------------------------------------
  def order_url(field_name, filter_params={})
    filter_params = (filter_params.to_h || {}).dup
    filter_params[:order] = field_name
    filter_params[:direction] = (filter_params[:direction] == 'asc') ? 'desc' : 'asc'
    return {filter: filter_params}
  end

  #---------------------------------------------------------------
  def field_data(id, fields)
    {id: id, fields: fields.gsub("\n", "").gsub('"', "'")}
  end

  #---------------------------------------------------------------
  def from_filter?
    params[:filter].present? && !params[:filter].key?("direction") && !params[:filter].key?("order")
  end

end